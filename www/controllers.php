<?php

/*
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once('models.php');

class Controller {
	protected function _view($out) {
		$out['logged'] = false;
		$out['admin'] = false;
		$out['user'] = '';

		if (isset($_SESSION['suitup_cookie'])) {
			$data = explode(':', $_SESSION['suitup_cookie']);
			if ($this->_checkLogin())
			{
				$out['user'] = $data[0];
				$out['admin'] = 0;
				$out['logged'] = true;
			}
			if ($this->_checkAdminLogin()) {
				$admin = $this->_getAdminLogin();
				$out['user'] = $data[0];
				$out['admin'] = $admin['type'];
				$out['logged'] = true;
			}
		}
		echo loadView('views/layout.php', $out);
	}	
	public function error404() {
		$out['content'] = loadView('views/404.php');
		$this->_view($out);
	}
	public function error403() {
		$out['content'] = loadView('views/403.php');
		$this->_view($out);
	}

	protected function _checkLogin() {
		$model = new Users_Model();

		if(!isset($_SESSION['suitup_cookie']))
 		{
 			return false;
 		}
 		$data = explode(':', $_SESSION['suitup_cookie']);
		$result = $model->getUser($data[0], $data[1]);
		foreach ($result as $row) {
			if ($row['login']==$data[0] && $row['password']==$data[1]) {
				return true;
			} else {
				return false;
			}
		}
		$result->closeCursor();
		return false;
	}

	protected function _getLogin() {
		$model = new Users_Model();

		if(!isset($_SESSION['suitup_cookie']))
 		{
 			return 0;
 		}
 		$data = explode(':', $_SESSION['suitup_cookie']);
		$result = $model->getUser($data[0], $data[1]);
		foreach ($result as $row) {
			if ($row['login']==$data[0] && $row['password']==$data[1]) {
				return $row;
			} else {
				return 0;
			}
		}
		$result->closeCursor();
		return 0;
	}

	protected function _checkAdminLogin() {
		$model = new Admins_Model();

		if(!isset($_SESSION['suitup_cookie']))
 		{
 			return false;
 		}
 		$data = explode(':', $_SESSION['suitup_cookie']);
		$result = $model->getAdmin($data[0], $data[1]);
		foreach ($result as $row) {
			if ($row['login']==$data[0] && $row['password']==$data[1]) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	protected function _getAdminLogin() {
		$model = new Admins_Model();

		if(!isset($_SESSION['suitup_cookie']))
 		{
 			return 0;
 		}
 		$data = explode(':', $_SESSION['suitup_cookie']);
		$result = $model->getAdmin($data[0], $data[1]);
		foreach ($result as $row) {
			if ($row['login']==$data[0] && $row['password']==$data[1]) {
				return $row;
			} else {
				return 0;
			}
		}
		$result->closeCursor();
		return 0;
	}
}

class Home_Controller extends Controller {
	public function index() {
		$out['content'] = loadView('views/home.php');
		$this->_view($out);
	}

	public function sendOrder() {
		$orders = new Orders_Model();
		$items = new Items_Model();

		$user = $this->_getLogin();
		if ($user==0) {
			echo 'Unexpected error';
			die;
		}

		$orders->addOrder($user['id'], htmlspecialchars($_POST['itemId']), htmlspecialchars($_POST['address']), htmlspecialchars($_POST['time']));
		$items->makeUnavaible(htmlspecialchars($_POST['itemId']));

		header('location: order-success');
	}

	public function orderSuccess() {
		$out['content'] = loadView('views/orderSuccess.php');
		$this->_view($out);
	}

	public function showItem() {
		$model = new Items_Model();
		$result = $model->getItem($_GET['id']);
		$data = array();
		foreach ($result as $row) {
			$data = $row;
		}
		$usrData = $this->_getLogin();
		$data['userAddress'] = $usrData['address'];
		$data['orderPosibility'] = false;
		if ($usrData!=0) {
			$data['orderPosibility'] = true;
		}
		$out['content'] = loadView('views/item.php', $data);
		$result->closeCursor();
		$this->_view($out);
	}

	public function browse() {
		$model = new Items_Model();
		$result = $model->getAllItems();

		$data['result'] = $result;
		$out['content'] = loadView('views/browse.php', $data);

		$result->closeCursor();

		$this->_view($out);
	}

	public function searchForm() {
		$out['content'] = loadView('views/searchForm.php');
		$this->_view($out);
	}

	public function search() {
		$model = new Items_Model();

		if (!isset($_POST['label'])) $_POST['label']='';
		if (!isset($_POST['type'])) $_POST['type']='';
		if (!isset($_POST['size'])) $_POST['size']='';
		if (!isset($_POST['color'])) $_POST['color']='';
		if (!isset($_POST['prize'])) $_POST['prize']='';

		$result = $model->getSearched(htmlspecialchars($_POST['label']), htmlspecialchars($_POST['type']), htmlspecialchars($_POST['size']), htmlspecialchars($_POST['color']), htmlspecialchars($_POST['prize']));

		$data['result'] = $result;
		$out['content'] = loadView('views/browse.php', $data);
		if ($result->rowCount()==0) {
			$out['content'] = '<h2>Brak wyników dla podanych parametrów.</h2>';
		}

		$result->closeCursor();

		$this->_view($out);
	}

	public function profile() {
		$row = $this->_getLogin();
		if ($row==0) {
			$out['content'] = loadView('views/403.php');
		} else {
			$out['content'] = loadView('views/profile.php', $row);
		}
		if ($row==0) {
			$row = $this->_getAdminLogin();
			if ($row==0) {
				$out['content'] = loadView('views/403.php');
			} else {
				$out['content'] = loadView('views/adminProfile.php', $row);
			}
		}
		$this->_view($out);
	}

	public function loginForm() {
		$out['content'] = loadView('views/loginForm.php');
		$this->_view($out);
	}

	public function registerForm() {
		$out['content'] = loadView('views/registerForm.php');
		$this->_view($out);
	}

	public function login() {
		$_SESSION['suitup_cookie'] = htmlspecialchars($_POST['login']).':'.md5(htmlspecialchars($_POST['password']));
		header('location: home');
	}

	public function logout() {
		$_SESSION['suitup_cookie'] = ':';
		header('location: home');
	}

	public function register() {
		$model = new Users_Model();
		if (!$this->_checkLogin()) {
			$model->addUser($_POST['login'], md5($_POST['password']), htmlspecialchars($_POST['name']), htmlspecialchars($_POST['surname']), htmlspecialchars($_POST['phone']), htmlspecialchars($_POST['address']));
			header('location: register-success');
		}
	}

	public function registerSuccess() {
		$out['content'] = loadView('views/registered.php');
		$this->_view($out);
	}

	public function updateProfile() {
		$model = new Users_Model();
		if ($this->_checkLogin()) {
			$user = explode(':', $_SESSION['suitup_cookie']);
			$model->updateUser($user[0], $user[1], htmlspecialchars($_POST['name']), htmlspecialchars($_POST['surname']), htmlspecialchars($_POST['phone']), htmlspecialchars($_POST['address']));
			header('location: profile');
		} else {
			$out['content'] = loadView('views/403.php');
			$this->_view($out);
		}
	}

	public function changeUserPassword() {
		$model = new Users_Model();
		if ($this->_checkLogin()) {
			$user = explode(':', $_SESSION['suitup_cookie']);
			$model->updatePasswordForUser($user[0], md5(htmlspecialchars($_POST['newPassword'])));
			$_SESSION['suitup_cookie'] = $user[0].':'.md5(htmlspecialchars($_POST['newPassword']));
			header('location: profile');
		} else {
			$out['content'] = loadView('views/403.php');
			$this->_view($out);
		}
	}
}

class Admin_Controller extends Controller {
	public function _throwOut() {
		if (!$this->_checkAdminLogin()) {
			$this->error403();
			die;
		}
	}
	public function _strongThrowOut() {
		$this->_throwOut();
		$admin = $this->_getAdminLogin();
		if ($admin['type']!=1) {
			$this->error403();
			die;
		}
	}

	public function index() {
		$this->_throwOut();
		$out['content'] = '';
		$this->_view($out);
	}

	public function orders() {
		$this->_throwOut();
		$admin = $this->_getAdminLogin();
		$data['adminType'] = $admin['type'];

		$model = new Orders_Model();
		$data['result'] = $model->getAllOrders();
		$model = new PostedOrders_Model();
		$data['posted'] = $model->getAllOrders();
		$out['content'] = loadView('views/admin/orders.php', $data);
		$this->_view($out);	
	}

	public function items() {
		$this->_throwOut();
		$admin = $this->_getAdminLogin();
		$data['adminType'] = $admin['type'];

		$model = new Items_Model();
		$data['result'] = $model->getAllItems();
		$out['content'] = loadView('views/admin/items.php', $data);
		$this->_view($out);	
	}

	public function users() {
		$this->_throwOut();
		$admin = $this->_getAdminLogin();
		$data['adminType'] = $admin['type'];

		$model = new Users_Model();
		$data['result'] = $model->getAllUsers();
		$out['content'] = loadView('views/admin/users.php', $data);
		$this->_view($out);
	}

	public function workers() {
		$this->_strongThrowOut();
		$admin = $this->_getAdminLogin();
		$data['adminType'] = $admin['type'];

		$model = new Admins_Model();
		$data['result'] = $model->getAllAdmins();
		$out['content'] = loadView('views/admin/workers.php', $data);
		$this->_view($out);
	}

	public function phoneOrder() {
		$this->_throwOut();
		$admin = $this->_getAdminLogin();
		$data['adminType'] = $admin['type'];
		$out['content'] = loadView('views/admin/orders.php');
		$this->_view($out);	
	}

	public function realizeOrder() {
		$this->_throwOut();
		$model = new Orders_Model();
		$model->realizeOrder(htmlspecialchars($_GET['orderId']));
		header('location: admin-orders');
	}

	public function finalizeOrder() {
		$this->_throwOut();
		$itemsModel = new Items_Model();
		$items = $itemsModel->getItem($_GET['itemId']);
		foreach ($items as $item) {
			$model = new Orders_Model();
			$model->finalizeOrder(htmlspecialchars($_GET['orderId']), htmlspecialchars($_GET['time']), htmlspecialchars($item['prize']));
		}
		header('location: admin-orders');
	}

	public function cancelOrder() {
		$this->_throwOut();
		$model = new Orders_Model();
		$model->cancelOrder(htmlspecialchars($_GET['orderId']));
		header('location: admin-orders');
	}

	public function makeAvaible() {
		$this->_throwOut();
		$items = new Items_Model();
		$items->makeAvaible(htmlspecialchars($_GET['itemId']));
		header('location: admin-items');
	}

	public function phoneOrderForm() {
		$this->_throwOut();
		$userModel = new Users_Model();
		$users = $userModel->getUserById(htmlspecialchars($_GET['userId']));
		foreach ($users as $data) {
			$out['content'] = loadView('views/admin/phoneOrder.php', $data);
		}
		$this->_view($out);
	}

	public function sendOrder() {
		$orders = new Orders_Model();
		$items = new Items_Model();
		$users = new Users_Model();
		$result = $users->getUser(htmlspecialchars($_POST['userLogin']), md5(htmlspecialchars($_POST['userPassword'])));
		if ($result->rowCount()==0) {
			$this->error403();
			die;
		}

		$orders->addOrder(htmlspecialchars($_POST['userId']), htmlspecialchars($_POST['itemId']), htmlspecialchars($_POST['address']), htmlspecialchars($_POST['time']));
		$items->makeUnavaible(htmlspecialchars($_POST['itemId']));

		header('location: admin-orders');
	}

	public function deleteUser() {
		$this->_strongThrowOut();
		$model = new Users_Model();
		$model->deleteUser($_GET['userId']);
		header('location: admin-users');
	}

	public function deleteItem() {
		$this->_throwOut();
		$items = new Items_Model();
		$items->deleteItem(htmlspecialchars($_GET['itemId']));
		header('location: admin-items');
	}

	public function addWorkerForm() {
		$this->_strongThrowOut();
		$out['content'] = loadView('views/admin/addWorker.php');
		$this->_view($out);
	}

	public function addWorker() {
		$this->_strongThrowOut();

		$model = new Admins_Model();
		$model->addAdmin(htmlspecialchars(($_POST['owner'])), 2, htmlspecialchars($_POST['login']), md5(htmlspecialchars($_POST['password'])));

		header('location: admin-workers');
	}

	public function updateWorkerForm() {
		$this->_strongThrowOut();
		$adminId = $_GET['adminId'];
		$model = new Admins_Model();
		$result = $model->getAdminById($adminId);
		foreach ($result as $row) {
			$out['content'] = loadView('views/admin/updateWorker.php', $row);
		}
		$this->_view($out);
	}

	public function updateWorker() {
		$this->_strongThrowOut();

		$model = new Admins_Model();
		$type = 2;
		if ($_POST['type']=='Administrator') {
			$type = 1;
		}
		$model->updateAdmin(htmlspecialchars($_POST['owner']), $type, htmlspecialchars($_POST['login']), htmlspecialchars($_POST['password']));
		header('location: admin-workers');
	}

	public function updateWorkerPassword() {
		$this->_throwOut();
		$model = new Admins_Model();
		$model->updatePasswordForAdmin(htmlspecialchars($_POST['login']), md5(htmlspecialchars($_POST['newPassword'])));
		$_SESSION['suitup_cookie'] = htmlspecialchars($_POST['login']).':'.md5(htmlspecialchars($_POST['newPassword']));
		header('location: admin-workers');
	}

	public function updateAdminProfile() {
		$this->_strongThrowOut();
		$model = new Admins_Model();
		$type = 2;
		if ($_POST['type']=='Administrator') {
			$type = 1;
		}
		$model->updateAdmin(htmlspecialchars($_POST['owner']), $type, htmlspecialchars($_POST['login']), htmlspecialchars($_POST['password']));
		header('location: profile');
	}

	public function updateAdminPassword() {
		$this->_strongThrowOut();
		$model = new Admins_Model();
		$model->updatePasswordForAdmin(htmlspecialchars($_POST['login']), md5(htmlspecialchars($_POST['newPassword'])));
		$_SESSION['suitup_cookie'] = htmlspecialchars($_POST['login']).':'.md5(htmlspecialchars($_POST['newPassword']));
		header('location: profile');
	}

	public function addUserForm() {
		$this->_throwOut();
		$out['content'] = loadView('views/admin/addUser.php');
		$this->_view($out);
	}

	public function addUser() {
		$this->_throwOut();
		$model = new Users_Model();
		$model->addUser($_POST['login'], md5($_POST['password']), htmlspecialchars($_POST['name']), htmlspecialchars($_POST['surname']), htmlspecialchars($_POST['phone']), htmlspecialchars($_POST['address']));
		header('location: admin-users');
	}

	public function addItemForm() {
		$this->_throwOut();
		$out['content'] = loadView('views/admin/addItem.php');
		$this->_view($out);	
	}

	public function addItem() {
		$this->_throwOut();
		$model = new Items_Model();
		$model->addItem(htmlspecialchars($_POST['label']), htmlspecialchars($_POST['color']), htmlspecialchars($_POST['size']), htmlspecialchars($_POST['type']), htmlspecialchars($_POST['prize']), htmlspecialchars($_POST['img']));
		header('location: admin-items');
	}

	public function editItemForm() {
		$this->_throwOut();
		$model = new Items_Model();
		$result = $model->getItem($_GET['id']);
		$data = array();
		foreach ($result as $row) {
			$data = $row;
		}
		$out['content'] = loadView('views/admin/editItem.php', $data);
		$result->closeCursor();
		$this->_view($out);
	}

	public function editItem() {
		$this->_throwOut();
		$model = new Items_Model();
		$model->updateItem(htmlspecialchars($_POST['id']), htmlspecialchars($_POST['label']), htmlspecialchars($_POST['color']), htmlspecialchars($_POST['size']), htmlspecialchars($_POST['type']), htmlspecialchars($_POST['prize']), htmlspecialchars($_POST['img']));
		header('location: admin-items');
	}
}