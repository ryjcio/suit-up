<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<link href="static/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="container">
	<?php
		require ('utilities.php');
		
		if (!isDevelopmentOn()) die;
		
		try {
			$db = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'], $config['db_user'], $config['db_password']);
		}
		catch (PDOException $e) {
			print "Błąd połączenia z bazą! : " . $e->getMessage() . "<br />";
			die();
		}

		$addAdmin = 'INSERT INTO `'.$config['db_name'].'`.`'.$config['db_prefix']."admins` (`id`, `login`, `password`, `owner`, `type`) VALUES (NULL, 'worker', '".md5('qwerty')."', 'Marek Prądzyński', '2');";
		$result = $db->query($addAdmin);
		if (!$result) echo 'Błąd przy dodawaniu pracownika<br />';
		else echo 'Pracownik dodany<br />';

		$names = array('Marek', 'Bartek', 'Łukasz', 'Aneta', 'Ania', 'Tomek' );
		$surnames = array('Miłoziąb', 'Karkówka', 'Żydożar', 'Mąciwgłąb', 'Szczerosiadł', 'Urżnąłek' );
		$logins = array('slodkie_bulki', 'malpeczka', 'kotek13', 'terefere', 'ramdamdam', 'cycuch89');
		$addresses = array('Karolkowa 13 m. 24 Warszawa', 'Foniczna 43 Wypizdówek', 'Barburkowa 13 m. 42 Melanżówek', 'Traktorzystki 2 m. 2 Grudziądz', 'Mekintosza 65 m. 12A Legnica', 'Słodkich tyłków 8 m. 12 Mińsk Mazowiecki');
		$password = md5('qwerty');

		for ($i=0; $i<6; $i++) {
			$addUser = 'INSERT INTO `'.$config['db_name'].'`.`'.$config['db_prefix']."users` (`id`, `login`, `password`, `name`, `surname`, `address`, `phone`) VALUES (NULL, '".$logins[$i]."', '".$password."', '".$names[$i]."', '".$surnames[$i]."', '".$addresses[$i]."', '784088321');";
			$result = $db->query($addUser);
			if (!$result) echo 'Błąd w dodawaniu użytkowników przy iteracji '.$i.'<br />';
			else echo 'Dodano klienta '.$i.'<br />';
		}
		

		$labels = array('Alice', 'Maggie', 'Agnes', 'April', 'Oh Honey', 'Robin', 'Zoey', 'Britney', 'Christina', 'Norah');
		$sizes = array('S', 'M', 'L', 'XL');
		$colors = array('czarny', 'szary', 'granatowy');
		$types = array('party', 'pogrzeb', 'ślub');
		$img = 'http://blogs.lexpress.fr/styles/le-boulevardier/wp-content/blogs.dir/845/files/2011/11/barney_stinson.jpg';
		$prize = 30;

		for ($i=0; $i<12; $i++) {
			$label = $labels[rand(0, 9)];
			$size = $sizes[rand(0, 3)];
			$color = $colors[rand(0, 2)];
			$type = $types[rand(0, 2)];
			$addItem = 'INSERT INTO `'.$config['db_name'].'`.`'.$config['db_prefix']."items` (`id`, `avaible`, `label`, `color`, `size`, `type`, `img`, `prize`)"."VALUES (NULL, '1', '".$label."', '".$color."', '".$size."', '".$type."', '".$img."', '".$prize."');";
			$result = $db->query($addItem);
			if (!$result) echo 'Błąd w dodawaniu przedmiotów przy iteracji '.$i.'<br />';
			else echo 'Dodano przedmiot '.$i.'<br />';		
		}
		echo 'KONIEC';
	?>
	</div>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>