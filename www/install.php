<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Instalacja</title>

	<link href="static/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="container">
	<?php

	require ('utilities.php');
	
	if (!isDevelopmentOn()) die;

	try {
		$db = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'], $config['db_user'], $config['db_password']);
	}
	catch (PDOException $e) {
		die("Błąd połączenia z bazą! : " . $e->getMessage() . "<br />");
	}

	echo 'Połączono z bazą danych <br />';

	$query = 'CREATE TABLE IF NOT EXISTS `'.$config['db_prefix'].'admins` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `login` varchar(255) NOT NULL,
	  `password` varchar(255) NOT NULL,
	  `owner` varchar(255) NOT NULL,
	  `type` int(2) unsigned NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE (`login`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

	$result = $db->query($query);
	if ($result!=false) {
		echo 'Utworzono tabelę admins<br />';
	}
	else {
		echo 'BŁĄD. Nie można utworzyć tabeli admins<br />';
	}
	
	$query = 'CREATE TABLE IF NOT EXISTS `'.$config['db_prefix'].'items` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `avaible` tinyint(1) NOT NULL,
	  `label` varchar(255) NOT NULL,
	  `color` varchar(255) NOT NULL,
	  `size` varchar(10) NOT NULL,
	  `type` varchar(255) NOT NULL,
	  `img` varchar(255) NOT NULL,
	  `prize` int(10) unsigned NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

	$result = $db->query($query);
	if ($result!=false) {
		echo 'Utworzono tabelę items<br />';
	}
	else {
		echo 'BŁĄD. Nie można utworzyć tabeli items<br />';
	}
	
	$query = 'CREATE TABLE IF NOT EXISTS `'.$config['db_prefix'].'orders` (
	  `id` int(10) NOT NULL AUTO_INCREMENT,
	  `address` varchar(255) NOT NULL,
	  `time` int(5) unsigned NOT NULL,
	  `group` int(10) unsigned NOT NULL,
	  `user_id` int(10) unsigned NOT NULL,
	  `item_id` int(10) unsigned NOT NULL,
	  PRIMARY KEY (`id`,`group`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

	$result = $db->query($query);
	if ($result!=false) {
		echo 'Utworzono tabelę orders<br />';
	}
	else {
		echo 'BŁĄD. Nie można utworzyć tabeli orders<br />';
	}
	
	$query = 'CREATE TABLE IF NOT EXISTS `'.$config['db_prefix'].'posted_orders` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `value` int(10) unsigned NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

	$result = $db->query($query);
	if ($result!=false) {
		echo 'Utworzono tabelę posted_orders<br />';
	}
	else {
		echo 'BŁĄD. Nie można utworzyć tabeli posted_orders<br />';
	}
	
	$query = 'CREATE TABLE IF NOT EXISTS `'.$config['db_prefix'].'users` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `login` varchar(255) NOT NULL,
	  `password` varchar(255) NOT NULL,
	  `name` varchar(255) NOT NULL,
	  `surname` varchar(255) NOT NULL,
	  `address` varchar(1024) NOT NULL,
	  `phone` varchar(15) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE (`login`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

	$result = $db->query($query);
	if ($result!=false) {
		echo 'Utworzono tabelę users<br />';
	}
	else {
		echo 'BŁĄD. Nie można utworzyć tabeli user<br />';
	}
	
	$addSuperadmin = 'INSERT INTO `'.$config['db_name'].'`.`'.$config['db_prefix']."admins` (`id`, `login`, `password`, `owner`, `type`) VALUES (NULL, 'admin', '".md5('qwerty')."', 'Janusz Przybyszewski', '1');";
	$result = $db->query($addSuperadmin);
	if ($result===false) echo 'Błąd<br />';
	echo 'Super admin dodany<br />';

	?>
	</div>

	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>
