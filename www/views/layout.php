<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->
<!doctype html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Suit up!</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/suitup.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Suit up!</a>
        </div>
        <nav class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="home">Start</a></li>
            <li><a href="browse">Przegladaj</a></li>
            <li><a href="search-form">Szukaj</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <?php
            if ($logged==false) : ?>
              <li><a href="login-form">Zaloguj się</a></li>
              <li><a href="register-form">Zarejestruj się</a></li>
            <?php endif;
            
            if ($admin!=0) : ?>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Panel&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="admin-orders">Zamówienia</a></li>
                <li><a href="admin-users">Klienci</a></li>
                <li><a href="admin-items">Towary</a></li>
                <?php if ($admin==1) : ?>
                  <li class="divider"></li>
                  <li><a href="admin-workers">Pracownicy</a></li>
                <?php endif; ?>
              </ul></li>
            <?php
            endif;
            if ($logged==true) : ?>
              <li><a href="profile"><?php echo $user; ?></a></li>
              <li><a href="index.php?action=logout">Wyloguj</a></li>
            <?php 
            endif;
            ?>
          </ul>
        </nav><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
    <!--Content-->
      <?php echo $content; ?>
    <!--END Content-->
    </div>
    <div class="container">
      <hr>

      <footer>
        <p>© Suit up 2014</p>
        <p>UWAGA! Aplikacja nie jest realnym sklepem, wszelkie działania i usługi oferowane na stronie są fikcyjne</p>
      </footer>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>