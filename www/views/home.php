<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->
    </div>
    <div class="jumbotron">
      <div class="container">
      	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        	<h1>Suit up!</h1>
        	<p>Witaj w naszej wypożyczalni garniturów. Wiemy jak wiele czasu trzeba poświęcić aby wyglądać reprezentatywnie. Zachęcamy więc do zapoznania się z naszą ofertą. Starannie dobieramy modele do aktualnych trendów i potrzeb klientów, więc na pewno znajdziesz coś dla siebie.</p>
        	<p><a class="btn btn-primary btn-lg" href="browse" role="button">Przeglądaj »</a></p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        	<img src="img/home-jumbotron.png" alt="jumbotron image" class="img-responsive">
        </div>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Zarejestruj się</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="register" role="button">Zarejestruj się »</a></p>
        </div>
        <div class="col-md-4">
          <h2>Każdy rozmiar</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="search-form" role="button">Szukaj »</a></p>
       </div>
        <div class="col-md-4">
          <h2>Czarny...</h2>
          <p>...pasuje do wszystkiego. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <form role="search" action="search" method="post">
            <input type="hidden" name="color" value="czarny">
            <p><button type="submit" class="btn btn-default" role="button">Przeglądaj »</button></p>
          </form>
        </div>
      </div>