<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->      

      <div class="row">
      <form class="form-signin col-lg-6 col-lg-push-3" role="search" action="search" method="post">
        <h2 class="form-signin-heading">Szukaj</h2>
        <label class="col-lg-2">Nazwa</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="" autofocus="" name="label">
        </div>
        <label class="col-lg-2">Kolor</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="" name="color">
        </div>
        <label class="col-lg-2">Rozmiar</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="" name="size">
        </div>
        <label class="col-lg-2">Typ</label>
        <div class="col-lg-10">
          <input type="phone" class="form-control" placeholder="" name="type">
        </div>
        <label class="col-lg-2">Cena</label>
        <div class="col-lg-10">
          <input type="login" class="form-control" placeholder="" name="prize">
        </div>
        <div class="col-lg-12">&nbsp;</div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Szukaj</button>
        <!-- <input type="hidden" value="search" name="action"> -->
      </form>
      </div>