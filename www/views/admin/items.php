<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h2 class="inline">Towary</h2><a href="admin-add-item" class="btn btn-success">Dodaj towar</a>
						<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<th>ID</th>
								<th>Dostępność</th>
								<th>Nazwa</th>
								<th>Kolor</th>
								<th>Rozmiar</th>
								<th>Typ</th>
								<th>Cena za godzinę</th>
								<th>Operacje</th>
							</thead>
							<tbody>
							<?php foreach ($result as $row) : ?>
								<tr>
									<td><?php echo $row['id'] ?></td>
									<td><?php if ($row['avaible']==0) : ?>
									  <span class="btn btn-warning">Niedostępny</span>
            			<?php else: ?>
              			<span class="btn btn-success">Dostępny</span>
           				<?php endif; ?> </td>
									<td><?php echo $row['label'] ?></td>
									<td><?php echo $row['color'] ?></td>
									<td><?php echo $row['size'] ?></td>
									<td><?php echo $row['type'] ?></td>
									<td><?php echo $row['prize'] ?> zł</td>
									<td>
										<a href="index.php?con=admin&action=deleteItem&itemId=<?php echo $row['id']; ?>" class="btn btn-danger" onclick="return confirm('Czy na pewno chcesz usunąć ten towar?');">Usuń</a>
										<a href="item-<?php echo $row['id']; ?>" class="btn btn-info">Pokaż</a>
										<a href="edit-item-<?php echo $row['id']; ?>" class="btn btn-primary">Edytuj</a>
										<a href="index.php?con=admin&action=makeAvaible&itemId=<?php echo $row['id']; ?>" class="btn btn-warning" onclick="return confirm('Czy na pewno chcesz udostępnić ten towar?');">Udostępnij</a>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						</div>
					</div>