<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
					<div class="table-responsive">
					<h2 class="inline">Klienci</h2><a href="admin-add-user" class="btn btn-success">Dodaj klienta</a>
					<table class="table table-hover">
						<thead>
							<th>ID</th>
							<th>Login</th>
							<th>Imię</th>
							<th>Nazwisko</th>
							<th>Adres</th>
							<th>Telefon</th>
							<th>Operacje</th>
						</thead>
						<tbody>
						<?php foreach ($result as $row) : ?>
							<tr>
								<td><?php echo $row['id'] ?></td>
								<td><?php echo $row['login'] ?></td>
								<td><?php echo $row['name'] ?></td>
								<td><?php echo $row['surname'] ?></td>
								<td><?php echo $row['address'] ?></td>
								<td><?php echo $row['phone'] ?></td>
								<td>
									<a href="admin-phone-order-<?php echo $row['id']?>" class="btn btn-success">Zamówienie telefoniczne</a>
									<a href="index.php?con=admin&action=deleteUser&userId=<?php echo $row['id']; ?>" class="btn btn-danger" onclick="return confirm('Czy na pewno chcesz usunąć tego użytkownika?');">Usuń</a>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
					</div>
				</div>