<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->
      <div class="row">
        <div class="item-wrapper item-page">
          <article class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
            <form class="form-signin col-lg-12" role="form" method="post" action="index.php?con=admin&action=editItem">
            <h2 class="form-signin-heading">Edytuj towar</h2>
            <label class="col-lg-2">Nazwa</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" placeholder="" required="" autofocus="" name="label" value="<?php echo $label; ?>">
            </div>
            <label class="col-lg-2">Kolor</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" placeholder="" required="" name="color" value="<?php echo $color; ?>">
            </div>
            <label class="col-lg-2">Rozmiar</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" placeholder="" required="" name="size" value="<?php echo $size; ?>">
            </div>
            <label class="col-lg-2">Typ</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" placeholder="" required="" name="type" value="<?php echo $type; ?>">
            </div>
            <label class="col-lg-2">Cena</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" placeholder="" required="" name="prize" value="<?php echo $prize; ?>">
            </div>
            <label class="col-lg-2">URL obrazka</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" placeholder="" required="" name="img" value="<?php echo $img; ?>">
            </div>
            <div>
              &nbsp;
              <input type="hidden" class="form-control" placeholder="" required="" name="id" value="<?php echo $id; ?>">
            </div>
            <button class="btn btn-lg btn-success" type="submit">Aktualizuj</button>
          </form>
          </article>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="<?php echo $img; ?>" alt="garnitur" class="img-responsive item-image">
          </div>
        </div>
      </div>