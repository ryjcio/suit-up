<!-- 
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
-->
					<div class="col-lg-12">
						<h2>Zamówienia</h2>
						<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<th>ID</th>
								<th>ID klienta</th>
								<th>ID przedmiotu</th>
								<th>Adres wysyłkowy</th>
								<th>Czas wypożyczenia</th>
								<th>Operacje</th>
							</thead>
							<tbody>
							<?php foreach ($result as $row) : ?>
								<tr>
									<td><?php echo $row['id'] ?></td>
									<td><?php echo $row['user_id'] ?></td>
									<td><?php echo $row['item_id'] ?></td>
									<td><?php echo $row['address'] ?></td>
									<td><?php echo $row['time'] ?> godzin</td>
									<td>
									<?php if ($row['group']==0) : ?>
										<a href="index.php?con=admin&action=realizeOrder&orderId=<?php echo $row['id']; ?>" class="btn btn-success">Realizuj</a>
										<a href="index.php?con=admin&action=cancelOrder&orderId=<?php echo $row['id']; ?>" class="btn btn-danger" onclick="return confirm('Czy na pewno chcesz anulować zamówienie?');">Anuluj</a>
									<?php else : ?>
										<a href="index.php?con=admin&action=finalizeOrder&orderId=<?php echo $row['id']; ?>&time=<?php echo $row['time']; ?>&itemId=<?php echo $row['item_id']; ?>" class="btn btn-success" onclick="return confirm('Czy na pewno chcesz usunąć sfinalizować zamówienie?');">Finalizuj</a>
									<?php endif; ?>
									</td>
								</tr>
							</tbody>
							<?php endforeach; ?>
						</table>
						</div>
					</div>
					<div class="col-lg-12">
						<h2>Historia operacji</h2>
						<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<th class="col-lg-1">ID</th>
								<th class="col-lg-11">Wartość</th>
							</thead>
							<tbody>
							<?php foreach ($posted as $row) : ?>
								<tr>
									<td><?php echo $row['id'] ?></td>
									<td><?php echo $row['value'] ?> zł</td>
								</tr>
							</tbody>
							<?php endforeach; ?>
						</table>
						</div>
					</div>