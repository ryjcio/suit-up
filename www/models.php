<?php

/*
  This file is part of Suit up application.

  Suit up application is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Suit up application is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Suit up application; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  Ten plik jest częścią Suit up application.

  Suit up application jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
  i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
  wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
  Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

  Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
  użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
  gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
  ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
  Powszechnej Licencji Publicznej GNU.

  Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
  Powszechnej Licencji Publicznej GNU (GNU General Public License);
  jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
  Place, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once('utilities.php');

class Items_Model {
	public function makeAvaible($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'items SET avaible=1 WHERE id='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function makeUnavaible($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'items SET avaible=0 WHERE id='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function addItem($label, $color, $size, $type, $prize, $img) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('INSERT INTO '.$config['db_prefix'].'items (label, color, size, type, prize, img, avaible) VALUES (\''.$label.'\', \''.$color.'\', \''.$size.'\', \''.$type.'\', \''.$prize.'\', \''.$img.'\', 0)');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function deleteItem($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('DELETE FROM '.$config['db_prefix'].'items WHERE id='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function updateItem($id, $label, $color, $size, $type, $prize, $img) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'items SET label=\''.$label.'\', color=\''.$color.'\', size=\''.$size.'\', type=\''.$type.'\', prize=\''.$prize.'\', img=\''.$img.'\' WHERE id='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function getAllItems() {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * from '.$config['db_prefix'].'items');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function getSearched($label, $type, $size, $color, $prize) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$query = 'SELECT * from '.$config['db_prefix'].'items ';
		$first = true;

		if ($label!='') {
			if ($first===true) {
				$first = false;
				$query = setStatementToQuery($query, 'label LIKE \''.$label.'\'');
			} 
			else {
				$query = addStatementToQuery($query, 'label LIKE \''.$label.'\'');
			}
		}
		if ($type!='') {
			if ($first===true) {
				$first = false;
				$query = setStatementToQuery($query, 'type LIKE \''.$type.'\'');
			}
			else {
				$query = addStatementToQuery($query, 'type LIKE \''.$type.'\'');
			}
		}
		if ($size!='') {
			if ($first===true) {
				$first = false;
				$query = setStatementToQuery($query, 'size = \''.$size.'\'');
			}
			else {
				$query = addStatementToQuery($query, 'size = \''.$size.'\'');
			}
		}
		if ($color!='') {
			if ($first===true) {
				$first = false;
				$query = setStatementToQuery($query, 'color LIKE \''.$color.'\'');
			}
			else {
				$query = addStatementToQuery($query, 'color LIKE \''.$color.'\'');
			}
		}
		if ($prize!='') {
			if ($first===true) {
				$first = false;
				$query = setStatementToQuery($query, 'prize = \''.$prize.'\'');
			}
			else {
				$query = addStatementToQuery($query, 'prize = \''.$prize.'\'');
			}
		}
		
		if ($query=='SELECT * from '.$config['db_prefix'].'items WHERE ') {
			$query = 'SELECT * from '.$config['db_prefix'].'items';
		}

		$result = $db->query($query);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function getItem($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * from '.$config['db_prefix'].'items WHERE id='.$id);

		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}
}

class Users_Model {
	public function getAllUsers() {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * FROM '.$config['db_prefix'].'users');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}

	public function getUser($login, $password) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * from '.$config['db_prefix'].'users WHERE login=\''.$login.'\' AND password=\''.$password.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function getUserById($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * from '.$config['db_prefix'].'users WHERE id=\''.$id.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}

	public function updateUser($login, $password, $name, $surname, $phone, $address) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'users SET name=\''.$name.'\', surname=\''.$surname.'\', phone=\''.$phone.'\', address=\''.$address.'\' WHERE login=\''.$login.'\' AND password=\''.$password.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function updatePasswordForUser($login, $newPassword) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'users SET password=\''.$newPassword.'\' WHERE login=\''.$login.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function addUser($login, $password, $name, $surname, $phone, $address) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('INSERT INTO '.$config['db_prefix'].'users (name, surname, phone, address, login, password) VALUES (\''.$name.'\', \''.$surname.'\', \''.$phone.'\', \''.$address.'\', \''.$login.'\', \''.$password.'\')');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function deleteUser($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('DELETE FROM '.$config['db_prefix'].'users WHERE id=\''.$id.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}
}

class Admins_Model {
	public function getAdminByid($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * from '.$config['db_prefix'].'admins WHERE id=\''.$id.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}

	public function getAdmin($login, $password) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * from '.$config['db_prefix'].'admins WHERE login=\''.$login.'\' AND password=\''.$password.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function getAllAdmins() {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * FROM '.$config['db_prefix'].'admins');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}

	public function addAdmin($owner, $type, $login, $password) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('INSERT INTO '.$config['db_prefix'].'admins (owner, type, login, password) VALUES (\''.$owner.'\', \''.$type.'\', \''.$login.'\', \''.$password.'\')');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function updateAdmin($owner, $type, $login, $password) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'admins SET owner=\''.$owner.'\', type=\''.$type.'\' WHERE login=\''.$login.'\' AND password=\''.$password.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function updatePasswordForAdmin($login, $newPassword) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'admins SET password=\''.$newPassword.'\' WHERE login=\''.$login.'\'');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}
}

class Orders_Model {
	public function addOrder($userId, $itemId, $address, $time) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('INSERT INTO '.$config['db_prefix'].'orders (user_id, item_id, address, time) VALUES (\''.$userId.'\', \''.$itemId.'\', \''.$address.'\', \''.$time.'\')');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function getAllOrders() {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * FROM '.$config['db_prefix'].'orders');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}

	public function realizeOrder($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('UPDATE '.$config['db_prefix'].'orders SET `group`=1 WHERE `id`='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}

	public function finalizeOrder($id, $time, $prize) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('DELETE FROM '.$config['db_prefix'].'orders WHERE id='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		$result = $db->query('INSERT INTO '.$config['db_prefix'].'posted_orders (value) VALUES ('.$time * $prize.')');
		//echo 'INSERT INTO '.$config['db_prefix'].'posted_orders (value) VALUES ('.$time * $prize.')';
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

	public function cancelOrder($id) {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('DELETE FROM '.$config['db_prefix'].'orders WHERE id='.$id);
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;
	}

}

class PostedOrders_Model {
	public function getAllOrders() {
		global $config;
		$db = connectWithDatabase();
		if (!isset($db)) return 0;

		$result = $db->query('SELECT * FROM '.$config['db_prefix'].'posted_orders');
		if ((!$result) && (isDevelopmentOn())) echo 'Błąd przy pobieraniu danych z bazy<br />';

		return $result;	
	}
}
